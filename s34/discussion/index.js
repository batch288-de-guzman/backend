//user "reuire" directive to load the express module/package
//it will allow us to access methods and functions that will help us create easily our application or server

const express = require('express');


//create an application using express
//creates an express application and strores this in a constant variable called app
const app = express()

//for our application server to return, we need a port to listen to
const port = 4000;

//middleware
    //is software that provides common services and capabilities to cpplication outside of what's offred by ther operating system
    //allows our application  to read json data

    app.use(express.json());

    //this one will allow us to read data from forms.
    //by default, information recieved from the url can only be recieved as a string or an array

    app.use(express.urlencoded({extended: true}));

    //routes
    //express has methods corresponding to each https method

    app.get('/', (request, response) => {
        //once the route is accessed it will send a string respond containing 'hello world'

        // .end uses the node js module methods
        // .send method uses the expressjs module instead to send a response back to the client

        response.send("aishitenasai")
    })

    app.get('/hello', (request, response) => {

        response.send("pop up only on occasion brother")
    })

    //this route exprext to recieve a post request at the uri hello

    app.post('/hello',(request, response) => {
        console.log(request.body);

        response.send(`hello ${request.body.firstName}, ${request.body.lastName}`)
    })

 //this will server as a mock database
let users = [];

app.post('/signup', (request, response) => {
        console.log(request.body);

        if(request.body.username !== "" && request.body.password !== ""){
            users.push(request.body);

            response.end(`user ${request.body.username} successfully registered`)

        }else{
            response.send('fields must not be empty')
        }

})

    app.put('/change-password', (request, response) =>{

    let message;

        for(let index = 0; index < users.length; index++){

            //if the provided userbane ub tg client/psotman and the username of the current object in the loop is the same
            if(request.body.username == users[index].username){
                users[index].password = request.body.password

                message = `user ${request.body.username}'s password has been updated`

                break;
            }else{
                message = `user does not exist`
            }
        }

        response.send(message);
    })

    app.get('/home', (request, response)=>{

        response.send('Welcome to the home page')
    })


    app.get('/users', (request, response) => {
        response.send(users)
    })

    app.delete('/delete-user', (request, response)=>{

        let message;
        for (let index = 0; index < users.length; index++) {
            if(request.body.username == users[index].username){
                let user = users[index].unsername
                users.splice(0, 1)

                message = `user ${request.body.username} has been deleted`

                break;
            }
            if(message == undefined){
                message = `User does not exist`
                
            } else{
                message = `No users found`
            }
            
        }

        response.send(message)
    })






    //if the prot is accessed we can run the server

    //will return a message that the server is running at the assigned terminal

if(require.main === module){
    app.listen(port, ()=> console.log(`server running at localhost ${port}`))
}
module.exports = app;