
async function fruitsOnSale(db) {
	return await(


            db.fruits.aggregate([
                {$match: {onSale: true}},
                {$count: "onSale"}
            ])
		);
};


async function fruitsInStock(db) {
	return await(


            db.fruits.aggregate([
                {$match : {stock: {$gte : 20}}},
                {$count : "stock"}

            ])
		);
};



async function fruitsAvePrice(db) {
	return await(


            db.fruits.aggregate([
                {$match : {onSale: true}},
                {$group : {_id : "$onSale",fruitsAvePrice : {$avg: "$price"}}}
            ])
		);
};



async function fruitsHighPrice(db) {
	return await(


            db.fruits.aggregate([
                { $group : {_id : "$supplier_id", fruitsHighPrice : {$max :"$price"}, }}
            ])
		);
};




async function fruitsLowPrice(db) {
	return await(

            db.fruits.aggregate([
                { $group : {_id : "$supplier_id", fruitsLowPrice : {$min :"$price"}, }}
            ])
		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};
