// mongodb aggregation
/*
    used to generate manipulated data and perform operation to create filtered result that helps analyzing data
    compare to doing crud operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for result providing us with information to make necessary development decisions without having to create a frontend application
*/

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group : {_id: "$supplier_id", total: {
        $sum:"$stock"
    }}}
])

db.fruits.aggregate([
    {$match: {onSale:true}}
])


//$group groups the documents in terms of the id property declared in the _id property
db.fruits.aggregate([
    { $group : {_id: "$supplier_id", total: {
        $sum:"$stock"
    }}}
]) //grouped same supplier ids and added stock value


//max operator

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group : {_id : "$supplier_id", max : {$max :"$stock"}, sum : {$sum : "$stock"}}}
])

db.fruits.aggregate([

    { $group : {_id : "$color", max : {$max :"$stock"}, sum : {$sum : "$stock"}}}
])

//field projection with aggregations
/*
    the $projet operator can be used when aggregating data to exclude the returned result
*/

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group : {_id : "$supplier_id", max : {$max :"$stock"}, sum : {$sum : "$stock"}}},
    {$project: {_id: 0}}
])

//sorting aggregated result
//sorts the order of the aggregated result (-1 descending, 1 ascending) in terms of value of the value of the field 

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {
        _id: "$supplier_id",
        total: {$sum: "$stock"}
    }},
    {$sort : {total: 1}}
])


//_id declares how documents will be grouped
db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {
        _id: "$name",
        stocks: {$sum : "$stock"}
    }},
    {$sort : {_id: -1}}
])

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {
        _id: "$name",
        stocks: {$sum : "$stock"}
    }},
    {$project: {_id:0}},
    {$sort : {_id: -1}}
])

//Aggregating result base on an array field
    //unwind operator
    /*
        $unwind operator deconstructs an array field from a collection with an array value to output a result for each element
    */
db.fruits.aggregate([
    { $unwind : "$origin"}
])

//dispay fruit documents by their origin and kinds of fruit they supply

db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {
        _id: "$origin",
        kinds: {$sum: 1}
    }},
    {$sort: {kinds : 1, _id : 1  }}
])  