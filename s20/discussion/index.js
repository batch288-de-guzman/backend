console.log("gods plan")

//a while loop takes an expression/condition. expressions are any unit of code that can be evaluated as true or false. if the condition evaluates to be true, the statements/code block will be excecuted.

//loop will iterate certain number of times until an expression/condition is true
//iteration is the term giver to repitition of statements

let count = 1;
//while the statement of value count is not equal to zero the statement inside will run/iterate
    while(count !== 2){
        console.log("While : " + count);
        
        //decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
        //forgetting to include is BAD
        count++;
    }

//a do while loop works a lot like while loop. but unlike while loops, do-while loops guarantees that the code will be executed at least once
 /*
    syntax
        do{
            statement;
            incement/decrement;
        } while (condition/expression);
 */


// let number = Number(prompt("give me a number: "))

//         do{
//             console.log("do while: " + number)
//             number ++
//         } while (number < 10)

/*for loop a for loop is more flexible than while and do-while loops. it consists of 3 parts
        1 the initialization of value that will trac the progression of the loop
        2 exprerssion/condition that will be evaluated which will determine wether the loop will run one more time
        3 the "iteration " which indicates how to advance the loop

        syntax
            for(initializeation; expression/condition; iteration){
                statement;
            }
*/


        // will create a loop that start at 0 and end at 20
for ( let count = 0; count <= 20; count+=2){
    console.log("the current value of count is " + count)
}

let myString = "Alexys";

console.log(myString.length);

// console.log(myString[3]);

for(let index = 0; index < myString.length; index++){
    console.log(myString[index]);
}

let myName = "Alex";

for( index = 0; index < myName.length; index++){
    if(myName[index].toLowerCase() === "a" ||
        myName[index].toLowerCase()  === "i" ||
        myName[index].toLowerCase() === "u" ||
        myName[index].toLowerCase() === "e" ||
        myName[index].toLowerCase() === "o"
        ){
            console.log("");
        } else{
            console.log(myName[index]);
        }
}

let myNickname = "sento";

let updatedNickname = myNickname.replace("sento","sen");
console.log(updatedNickname);

//continue statement allows the code to go to the nex iteration of the loop without finishing the execution of all statements in a code
//break statement is used to terminate the current loop once a match has been found

for (let count = 0; count <= 20; count++){
    if(count >=10){
        console.log("the number is equal to 10, stopping the loop")
        break;
    }

    if(count % 2 === 0){
        console.log("the number is divisible to two,skipping . . .")
        continue;
    }
    console.log(count);

    
}

console.log("__________________")

let theName = "Alexandro";

for(let index = 0; index < theName.length; index++){
    if(theName[index] === "a"){
        console.log("continue to the iteration")
        continue;
    } else if(theName[index = "d"]){
        break;
    }else{
        console.log(theName[index]);
    }
}