console.log("sicko mode");

let grades = {
    math: 81,
    PE: 84,
    History: 90
};

console.dir(grades);

//Object is a data type that is used to represent real world objects. it is a collection of related data and/or fuctionalities

//creating objects using the object initializers
/*
    let objectName = {
        keyA: valueA,
        keyB: valueB
    }
*/

let cellphone = {
    name: "Nokia",
    ManufactureDate: 1999
}

let sampleArray = [1, 2, 3, 4, 5];
console.log(typeof sampleArray);
console.dir(sampleArray);

console.log("result from creating objects using initializers/literal notation");
console.log(cellphone);
console.log(cellphone);


//creating objects using class construction function
//creates a reusable function to create several objects that have the same data structures
//this is useful for creating multiple instances/copies of an object.

/*
    syntax:
    function objectName(valueA, ValueB){
        this.keyA = valueA;
        this.keyB = valueB;
    }
*/


//"this" keyword allows to assign a new object's properties by associating them with values recived from the constructor's function parameters.
function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}
//this is a unique instance of the Laptop object
/*
    the "new" operator creates an instance of an object
    objects and instances are ofter interchanged because ofject literals (let object={}) and instances (let object = new object) are distinct/ unique objects

*/

let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using object constructor");
console.log(laptop);

let newLaptop = new Laptop("Dell", 2017);
console.log("New laptop");
console.log(newLaptop);

let oldLaptop = Laptop("Portal R2E CCMC", 1980);
/*the example above invokes/calls the "Laptop" function instead of creating a new object instance
it returns the word undefines because the "Laptop" function does not have a return statement */
console.log("Old laptop");
console.log(oldLaptop);

//creating empty objects

let computer = {}
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

//accessing object properties
//using the dot notation

console.log("result from fot notation " + newLaptop.name + " " + newLaptop.manufactureDate);

//using square bracket notation
console.log("result from fot notation " + newLaptop["name"] + " " + newLaptop["manufactureDate"]);

//accessing array objects
/*
    acessing array elements can also be done usign square brackets
    accessing object properties using the square brackets notation and array indexes can cause confusion
    by using dot notation, this easily helps us differentiate accessing elements from arrays and properties from object
    object properties have names that make it easier to associate pieces of informations
*/

let array = [laptop, newLaptop];

//maybe confused from accessing array indexes
console.log(array[1]["name"]);
console.log(array[1].name);

//initializing/adding/deleting/reassignning object properties
/*
    like any other variables in javaScript, objects may have their properties initialized/added after the object was created/declared
    this is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};

car.name = "Honda Civic";

console.log("result from adding poperties using dot notation");
console.log(car);

//initializing/adding object properties using square brackets
/*
    while using square brackets it will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accessed using the square bracket notation
    this also makes names of object properties to not follow commonly used naming conventions for them
*/

car["Manufacture Date"] = 2019;
console.log(car["manufacture date"]);
console.log(car["Manufacture Date"]);
console.log(car.manufactureDate);
console.log("after square brackets");
console.log(car)

//deleting object properties
delete car["Manufacture Date"];
console.log(car);

//reassignning object properties
car.name = "Dodge Charger R/T";
console.log("result after reassigning")
console.log(car)

//object methods
/*
    a method is a function which is a property of an object
    they are also functions and one of the key differences  they have is that methods are fuctions related to a specific object
    mthods are useful for creating object specific fuctions which are used to perform task on them
    similar to functions/features of real world objects, on methods are defined based on what an object is capable of doing and how it shoul work
*/

let person = {
    name:"Hiki",
    talk : function(){
        console.log ("Atashi " + this.name + " desu!");
        // return "Atashi " + this.name + " desu!";
    }
}

console.log(person)
console.log("result from object methods")
person.talk()

//adding methods to objects
person.walk = function(){
    console.log(this.name + " walked 25 steps forward")
}

person.walk();

//methods are usefull for creating reusable functions that perform tasks related to objets

let friend = {
    firstName: "Tsunko",
    lastName: "Segs",
    address: {
        city: "Sapporo",
        country: "Japan",
    },
    emails: ['tsunkoconsplay@mail.com', "tsunkosegs69gmail.com"],
    aiSatsuNiShite: function(){
        console.log("atashi " + this.firstName + " " + this.lastName + " desu");
    }
};

console.log(friend);
friend.aiSatsuNiShite();

//real world application of objects
/*
    scenario
        1. we would liek to create a game that would have serveral pokemon interact with each other
        2. every pokemon would have the same set of stats, properties and fuctions
*/


//using object literals to create multiple pokemons would be time consuming
let myPokemon = {
    name: "Pikachu",
    leve: 3,
    health: 100,
    attack: 50,
    tackle : function(){
        console.log("This pokemon tackled targetPokemon");
        console.log("targetPokemon's health is now reduced to targetPokemonHealt")
    }
}

console.log(myPokemon);

//creating object constructor will help in this process

function Pokemon(name, level){
    //properties
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = level

    //methods
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s" + " Health is now reduced to targetPokemonHealth")
    };
    this.faint = function(){
        console.log(this.name + " fainted ")
    }
};

let pikachu = new Pokemon("Pikachu", 16);
let gardevoir = new Pokemon("Gardevoir", 67);


pikachu.tackle(gardevoir);