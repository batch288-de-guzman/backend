// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

let trainerObject = {
    name: "Ash Ketchum",
    age: 10,
    pokemon:[
        "Pikachu",
        "Charizard",
        "Squirtle",
        "Bulbasaur"        
    ],
    friends: {
        hoenn : ["Max", "May"],
        kanto : ["Misty", "brock"]
    }
}

console.log(trainerObject)

// Initialize/add the given object properties and methods

let trainer = {
    // Properties
    pokemon:"Pikachu",
    // Methods
    talk : function(){
        // console.log(this.pokemon + "!"+ " I choose you!")
        return "Pikachu I choose you"
    }
}
console.log("Result of talk method")
trainer.talk()


// Check if all properties and methods were properly added

// Access object properties using dot notation

console.log(trainerObject.name);

// Access object properties using square bracket notation

console.log(trainerObject.pokemon);

// Access the trainer "talk" method

trainer.talk()

// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level){
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    this.tackle = function(target){
        target.health = (target.health - this.attack);
        console.log(this.name + " tackled " + target.name);
        console.log(target.name + "'s" + " health is now reduced to " + target.health);
        if (target.health <= 0){
            target.faint(target);
           return target
        }
    };
    this.faint = function(target){
        console.log(target.name + " fainted")
    }
}

// Create/instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", 12);

console.log(pikachu);

// Create/instantiate a new pokemon
let geodude = new Pokemon("Geodude", 8);

console.log(geodude);

// Create/instantiate a new pokemon
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(mewtwo);

// Invoke the tackle method and target a different object

geodude.tackle(pikachu);

// Invoke the tackle method and target a different object
 mewtwo.tackle(geodude);









//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}