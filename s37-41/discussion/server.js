const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require(`./routes/userRoutes.js`)

//it will allow our backend application to be available to our frontend application
//it will also all us to control the app's cross origin resource sharing settings
const cors = require('cors')
const coursesRoutes = require(`./routes/coursesRoutes.js`)


const port = 4001;
const app = express();

//mongodb connection
//establish the connection between the db and the application or server
//the name of the database should be course booking API
mongoose.connect(`mongodb+srv://admin:admin@batch288deguzman.zygjru8.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority`, {useNewUrlParser: true, useUnifiedTopology:true})

let db = mongoose.connection;

db.on(`error`, console.error.bind(console, `connection error`));
db.once(`open`, ()=>console.log(`connected to database`))

//______middleware______
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//add routing for userRoute below
app.use(`/user`, userRoutes);

app.use(`/courses`, coursesRoutes);

app.listen(port, ()=> console.log(`server running at port ${port}`))