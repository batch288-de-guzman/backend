const User = require(`../models/User.js`);

const Courses = require(`../models/Courses.js`);

const bcrypt = require(`bcrypt`)

//require auth.js
const auth = require(`../auth.js`);

//controllers
//create a controller for the signup
    //1. validate wether the user is existing or not
module.exports.registerUser = (request, response)=>{
    User.findOne({email: request.body.email})
    .then(result => {
        if(result){
            return response.send(`${request.body.email} has been taken! Try Logging in or use different email in signing up!`)
        }else{
            let newUser = new User({
                firstName : request.body.firstName,
                lastName : request.body.lastName,
                email : request.body.email,
                password : bcrypt.hashSync(request.body.password, 10),
                isAdmin : request.body.isAdmin,
                mobileNo : request.body.mobileNo
            })

            //save the new user
            //error handling
            newUser.save()
            .then(saved => response.send(`${request.body.email} is now registered`))
            .catch(error => response.send(error))
        }
    }).catch(error => response.send(error))
}


//new controller for the authentication
module.exports.loginUser = (request, response) => {

    User.findOne({email : request.body.email})
    .then(result =>{
        if(!result){
            return response.send(`${request.body.email} is not yet registered`)
        }else{
            //the compareSync method is used to compare a non encrypted password from the login form to the encrypted password retieve from the find method. returns true or false depending on the result of the comparison
            const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
            
            if(isPasswordCorrect){
                return response.send({
                    auth: auth.createAccessToken(result)
                })
            }else{
                return response.send(`incorrect password `)
            }
        }
    }).catch(error => response.send(error));
}


module.exports.getProfile = (request, response) => {

    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin){
        return User.findById(request.body.id)
    .then(result =>{
        password : result.password = `******`
        response.send(result)

    }).catch(error => response.send(error))
    }else{
        return response.send(`you are not an admin. you dont have access to this route.`)
    }
    
}


//controller for enroll course.

module.exports.enrollCourse = (request, response) =>{
    
    const courseId = request.body.id;
    const userData = auth.decode(request.headers.authorization);

        //push to user document
    if(userData.isAdmin){
        return response.send(`Admin cannot enroll to a course.`)
    }else{
        let isUserUpdated = User.findOne({_id: userData.id})
        .then(result => {result.enrollments.push({
            courseId : courseId
        })
        result.save()
        .then(save => true)
        .catch(error => false)
    })
    .catch(error =>false)

    let isCourseUpdated = Courses.findOne({_id: courseId})
    .then(result =>{
        result.enrollees.push({userId: userData.id})
        result.save()
        .then(save => true)
        .catch(error => false)
    })
    .catch(error => false)

    if(isUserUpdated && isCourseUpdated){
        return response.send(`enrollment is successful`)
    }else{
        return response.send(`There was an error in the enrollment please try again`)
    }

    }
}