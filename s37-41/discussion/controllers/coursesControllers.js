const Courses = require(`../models/Courses.js`);
const auth = require(`../auth.js`);
const response = require (`express`);

let notAdminMsg = `You don't have access to this route`

module.exports.addCourse = (request, response) =>{
    
    const userData = auth.decode(request.headers.authorization);
    if(userData.isAdmin){
         //create an object using the courses model
    let newCourse = new Courses({
        name: request.body.name,
        description: request.body.description,
        price: request.body.price,
        isActive: request.body.isActive,
        slots: request.body.slots
    })
    newCourse.save()
    .then(save => response.send(`Course successfully created`))
    .catch(error => response.send(error));
    }else{
        return response.send(`you are not an admin. you dont have access to this route.`)
    }
}


//in this controller we are going to retrieve all of the courses in our database 

module.exports.getAllCourses = (request, response) => {

    const userData = auth.decode(request.headers.authorization);

    if(userData.isAdmin){
        Courses.find({})
    .then(result => response.send(result))
    .catch(error => response.send(error));  
    }else{
        return response.send(`You don't have access to thos route.`)
    }
    
}


//route for retrieving all active users

module.exports.getActiveCourses = (request, response) =>{
    
    Courses.find({isActive: true})
    .then(result => response.send(result))
    .catch(error => response.send(error));
}

//find by id
module.exports.getCourse = (request, response) =>{
    
    const courseId = request.params.courseId
    Courses.findById(courseId)
    .then(result => response.send(result))
    .catch(error => response.send(error));
}

//this controller will update a course document

module.exports.updateCourse = (request, response) =>{

        const userData = auth.decode(request.headers.authorization)
        const courseId = request.params.courseId

        let updatedCourse = {
            name: request.body.name,
            description: request.body.description,
            price: request.body.price
        }

        if(userData.isAdmin){
            Courses.findByIdAndUpdate(courseId, updatedCourse)
            .then(result => response.send(`successfully updated`))
            .catch(error => response.send(error));
        }else{
            return response.send(`You don't have access to this route`)
        }
}

//archive course
module.exports.archiveCourse = (request, response) => {
    
    const userData = auth.decode(request.headers.authorization)
    const courseId = request.params.courseId

    let archiveId = {isAdmin: request.body.isAdmin}

    if(userData.isAdmin){
        Courses.findByIdAndUpdate(courseId, archiveId)
        .then(result => response.send(`Course successfully archived`))
        .catch(error => response.send(error));
    }else{
        return response.send(notAdminMsg)
    }
    
}



module.exports.getInactiveCourses = (request, response)=>{

    const userData = auth.decode(request.headers.authorization);

    if(!userData.isAdmin){
        return response.send(notAdminMsg)
    }else{
        Courses.find({isActive: false})
        .then(result => response.send(result))
        .catch(error => response.send(error))
    }
}