const express = require(`express`);
const userControllers = require(`../controllers/userControllers.js`)

const auth = require(`../auth.js`)

const router = express.Router();


//routes
router.post(`/register`, userControllers.registerUser);

router.get(`/login`, userControllers.loginUser);

router.get(`/details`, auth.verify, userControllers.getProfile);

//token verification route

// router.get(`/verify`, auth.verify)

module.exports = router;


//route for course enrollment

router.post(`/enroll`, auth.verify, userControllers.enrollCourse);
