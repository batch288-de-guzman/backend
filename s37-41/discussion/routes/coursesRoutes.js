const express = require(`express`);
const coursesControllers = require(`../controllers/coursesControllers`);
const auth = require(`../auth.js`)

const router = express.Router();

router.post(`/addCourse`, auth.verify, coursesControllers.addCourse);

//route for retrieveing all courses
router.get(`/`, auth.verify, coursesControllers.getAllCourses);

//route for retrieving active coures
router.get(`/activeCourses`, coursesControllers.getActiveCourses);

//inactive courses
router.get(`/inactiveCourses`, auth.verify, coursesControllers.getInactiveCourses)





//________  ___without params

//route for gettng a specific course
router.get(`/:courseId`, coursesControllers.getCourse);

//route for updating course
router.patch(`/:courseId`,auth.verify, coursesControllers.updateCourse);

//archive a course
router.patch(`/:courseId/archive`, auth.verify, coursesControllers.archiveCourse);














module.exports = router;


