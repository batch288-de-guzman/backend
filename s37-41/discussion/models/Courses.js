//first require mongoose
const mongoose = require(`mongoose`);

const courseSchema = new mongoose.Schema({
    name : {
        type : String,
        required : [true, `Course name is required!`]
    },
    description : {
        type : String,
        required : [true, `Course description is required!`]
    },
    price : {
        type : Number,
        required : [true, `Course price is required!`]
    },
    isActive : {
        type : Boolean,
        required : [true, `Course status is required`]
    },
    createdOn : {
        type : Date,
        //the new date() expression instantiate a new date that stores the current date and time wheever a course is added
        default : new Date()
    },
    slots : {
        type: Number,
        required : [true, `Course slots is required`]
    },
    enrollees : [
        {
            userId : {
                type: String,
                required : [true, `User Id of the enrollee is required`]
            },
            enrolledOn : {
                type : Date,
                default : new Date()
            }
        }
    ]
})


//model section

const Courses = mongoose.model(`course`, courseSchema);

module.exports = Courses; 