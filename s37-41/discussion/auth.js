const jwt = require(`jsonwebtoken`);

//user defined string data that will be used to create json web token used in the algorthm for encrypting out data which makes it difficult to decode the information without the defined keyword
const secret = `CourseBookingAPI`


//section: JSON web token
//json web token or jwt is a way of securely passing information from the server to the frontend or to the parts of our server

//information is kept secure through the use of the secret code


//function for token creation

//the argument that will be passed in the parameter will be the document or object that contains the info of the user.


module.exports.createAccessToken = (user) => {
    const data = {
        id : user._id,
        isAdmin : user.isAdmin,
        email : user.email
    }
    //generate json web token using jwt sing method
    //generate the token using the form data and the secret code with no additional options provided
    return jwt.sign(data, secret, {});
}


//teoken verification

module.exports.verify = (request, response, next) =>{
    let token = request.headers.authorization;
    

    if(token !== undefined){
        token = token.slice(7, token.lenght)
        return jwt.verify(token, secret, (error, data) => {
            if(error){
                return response.send(`Auth failed`)
            }else{
                next();
            }
        })
    }else{
        return response.send(`No token provided`)
    }
}






module.exports.decode = (token) =>{
    token = token.slice(7, token.length);


    //the decode method is used to obtain the information from the jwt
    //the complete: true allows us to return additional information from the fwt program
    return jwt.decode(token, {complete: true}).payload;
}

