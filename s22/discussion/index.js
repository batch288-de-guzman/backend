// console.log("Good afternoon, Batch 288!");

//Array Methods
	// javaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	//Mutator Methods
	// Mutators methods are functions that "mutate" or change an array after they're created
	//These methods manipulate the original array performing various such as adding or removing elements.

	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

	// push()
		/*
			-Adds an element in the end of an array and returns the updated array's length
			Syntax:
				arrayName.push();
		*/

	console.log("Current array: ");
	console.log(fruits);

	let fruitsLength = fruits.push('Mango');

	console.log(fruitsLength);
	console.log('Mutated array from push method: ');
	console.log(fruits);

	// Pushing multple elements to an array

	fruitsLength = fruits.push('Avocado', 'Guava');

	console.log(fruitsLength);
	console.log("Mutated array after pushing multiple elements:");
	console.log(fruits);


	// pop()
	/*
		-removes the last element AND returns the removed element
		Syntax:
			arrayName.pop();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	let removedFruit = fruits.pop();

	console.log(removedFruit);
	console.log("Mutated Array from the pop method:");
	console.log(fruits);

	//unshift()
	/*
		-it adds one or more elements at the beginning of an array AND it returns the update array length
		-Syntax:
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementB ' . . .)
	*/

	console.log("Current Array:");
	console.log(fruits);

	fruitsLength = fruits.unshift('Lime', 'Banana');

	console.log(fruitsLength);
	console.log("Mutated array from unshift method: ");
	console.log(fruits);

	//shift()
	/*
		-removes an element at the beginning of an array AND returns the removed element.
		-syntax:
			arrayName.shift();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	removedFruit = fruits.shift();
	console.log(removedFruit);
	console.log("Mutated array from the shift method: ");
	console.log(fruits);

	// splice()
	/*
		-Simultaneously removes an elements from a specified index number and adds element
		- Syntax:
			arrayname.splice(startingIndex, deleteCount, elementsToBeAdded)
	*/
	console.log("Current Array: ");
	console.log(fruits);

	fruits.splice(fruits.length, 0, "Cherry");
	console.log("Mutated array after the splice method: ");
	console.log(fruits);

	//sort()
		/*
			Rearranges the array elements in alphanumeric order
				-syntax:
				arrayName.sort();
		*/
	console.log("Current Array: ");
	console.log(fruits);

	fruits.sort();

	console.log("Mutated array from the sort method: ");
	console.log(fruits);

    //reverse()
        // reserves the order of array elements 
    console.log("Current Array: ");
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated array from the reverse method: ");
	console.log(fruits);

    //not mutator methods are functions that do not modify or change an array after thay are create
    //these methods do not maniputalte the original array performing various tasks such asd returning elements from an array  and combining arrys and printing output


    let countries=["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

    //indexOf()
    //returns t he index number of the first matching elements found in a n array
    // if no match was fou, the reult will be -1
    //the search will be done from the first element preceeding to the last element

    let firstIndex = countries.indexOf("PH");
    console.log(firstIndex);

    let invalidCountry = countries.indexOf("BR");
    console.log(invalidCountry);

    firstIndex = countries.indexOf("PH", 2)
    console.log(firstIndex);

    console.log(countries)

    //lastIndexof()
    //returns the index number of the last matching element found in an array
    //the searching process will be done from last element proceeding to the first element

    let lastIndex = countries.lastIndexOf('PH')
    console.log(lastIndex);


    invalidCountry = countries.lastIndexOf("BR")
    console.log(invalidCountry)

    lastIndex = countries.lastIndexOf("PH", 4);
    console.log(lastIndex);

    console.log(countries);

    //slice method
    //portion/slices elements fron an array and returns a new array

let slicedArrayA = countries.slice(2);
console.log("result from slice method");
console.log(slicedArrayA);
//slicing off elements from a specified index to another index

let slicedArrayB = countries.slice(2,4)
console.log(slicedArrayB)

let slicedArrayC = countries.slice(-5)
console.log(slicedArrayC)

//toString
//returns an array as string separated by commas

let stringArray = countries.toString();
console.log(stringArray);
console.log(typeof stringArray);

//concat
//combines arrays to an array pr elements and return the combined result

let taskArrayA = ["drink HTML", "eat JavaScript"];
let taskArrayB = ["inhale CSS", "breath sass"];
let taskArrayC = ["get git", "be node"];

let task = taskArrayA.concat(taskArrayB);
console.log(task);

let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log(combinedTasks)


let allTask = taskArrayA.concat(taskArrayB, taskArrayC)
console.log(allTask)

let users = ["jonh", "jane", "joe", "robert"];

console.log(users.join());
console.log(users.join(""));
console.log(users.join ("-"));

//iteration methods are loops designed to perform repetitive tasks on arrays
//iteration methods loop over all items in an array
//usefull for manipulationg arraw data resulting in complex task
//array iteration methods normally works with a function supplied as an argument
//how these function works is by performing tasks that are pre-defined within an array's method

//forech()
//very similiar to for loop, but iterates on each of array element

console.log(allTask);

allTask.forEach(function(task){
    console.log(task);
});

//filtered task variable will hold all the elements from the alltask array that consist or have more than 10 characters
let filteredTask = [];

allTask.forEach(function(task){
    if(task.length > 10){
        filteredTask.push(task);
    }
})

console.log(filteredTask);

//map()

let numbers = [1, 2, 3, 4, 5];
console.log(numbers)

let numbersMap = numbers.map(function(number){
    return number * 3
});

console.log(numbersMap);

//every()
// checks if all the elements in an array meet the given condition
//returns true if elements meets conditions otherwise false

numbers = [1, 2, 3, 4, 5];

let allValid= numbers.every(function(number){
    return (number<6);
});

console.log(allValid);

//some()
//checks if at least one element in an array meets the given condition

let someValid = numbers.every(function(number){
    return (number > 17)
});

console.log(someValid)


//filter
//returns new arrays that contains elements which meets the given

numbers = [1, 2, 3, 4, 5];

let filterValid = numbers.filter(function(number){
    return( number % 2 === 0)
});

console.log(filterValid);

//includes()
// it checks if the argument passed can be found in the array 

let products = ["mouse", "keyboard", "laptop", "monitor"];

let productFound1 = products.includes("Mouse");

console.log(productFound1)

//reduce
//evaluates elements from left to right and returns or reduces into single value

numbers = [1, 2, 3, 4, 5];

//the first parameter in the function will be the accumulator
//the scoed paramenter in the function will be the current value

let reducedArray = numbers.reduce(function(x,y){
    console.log("accumulator " + x);
    console.log("current value " + y);
    return x*y;
})
console.log(reducedArray)

