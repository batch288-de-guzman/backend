// show databases - list of the db inside our cluster

//use "dbname" - to use a specifix database
//show collections - to see the list of collection inside the db

//CRUD operation
/*
    crud operation is the heart of any backend application
    mastering the crud operation is essential for any developer ex
*/

//inserting document
/*
    since mongodb deals with objects as its structure for our document we can easily creat them by providing objects on our method/operation

    syntax:
        db.collectionName.insertOne({
            object
        })
*/

db.users.insertOne({
    firstName : "Sagiri",
    lastName: "Izumi",
    age: 17,
    contact: {
        phone: "1321321321",
        email: "sagiri@gmail.com"
    },
    courses:["css", "javascript"],
    department: "none"
});

//insert many 
/*
    syntax:
    db.collectionName.insertMany([{
        objectB
    }]);
 */


db.users.insertMany([
    {
        firstName: "Chisa",
        lastName: "Kotegawa",
        age: 19,
        contact:{
            phone: "987654321",
            email: "chisaChiisai@gmail.com"
        },
        courses: ["Diving", "Marine Biology", "Evinronment Preservation"],
        department: "none"
    },
    {
        firstName: "Ginko",
        lastName: " Sora",
        age: 16,
        contact: {
            phone: "987654321",
            email: "ginkokawaii@gmail.com"
        },
        courses:["react", "laravel", "sass"],
        department: "none"
    }
]);


db.userss.insertOne({
    firstName : "Sagiri",
    lastName: "Izumi",
    age: 17,
    contact: {
        phone: "1321321321",
        email: "sagiri@gmail.com"
    },
    courses:["css", "javascript"],
    department: "none"
});

//Finding document (read) operation
/*
    db.collectionName.find();
    db.collectionName.find({field:value});
*/

//using find method will show you the list of all the documents inside our collection
db.users.find().pretty();

db.users.find({firstName:"Chisa"});

db.users.find({"_id" : ObjectId("646c55d64ce3407aac0f1ad8")});

db.users.find({firstName:"Sagiri"});

db.users.find({"contact.phone": "987654321"});

db.users.insertOne({
    firstName : "test",
    lastName: "test",
    age: 0,
    contact: {
        phone: "000000",
        email: "test@gmail.com"
    },
    courses:[],
    department: "none"
});

db.users.updateOne(
    {firstName: "test"},
    {
        $set:{
            firstName: "Yukina",
            lastName: "Minato",
            age: 19,
            contact: {
                phone:"123345567",
                email:"songstres@gmail.com"
            },
            courses: ["Voice Lessons", "Become Hot", "Being Hot"],
            department: "none",
            status:"none",
        }
    }
);

db.users.updateOne(
    {firstName: "Yukina"},
    {
        $set:{
            firstName: "Yukinya"
        }
    }
);

db.users.updateMany(
    {department: "none"},{
        $set: {
            department: "HR"
        }
    }
);

db.users.insertOne({firstName: "test"});

db.users.replaceOne(
    {firstName: "test"},{
        firstName: "Billy",
        lastName: "Joel",
        age: 65,
        contact: {
            phone: "321321321",
            email: "billyjeans@gamil.com"
        },
        courses: ["asdasd", "asdasd"],
        department: "none"
    }
);

//deleting document

db.users.deleteOne({
    firstName: "Billy"
});

//deleting multiple documents
db.userss.deleteMany({firstName: "Sagiri"});

db.users.deleteMany({});