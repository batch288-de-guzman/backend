//Add code here
const http = require("http");


let app = http.createServer(function(request, response){
    if(request.url == "/profile" && request.method == "GET"){
        response.writeHead(200,{"content-type" : "text/plain"});

        response.end("Welcome to your Profile");

    } else if(request.url=="/courses" && request.method == "GET"){
        response.writeHead(200,{"content-type": "text/plain"});

        response.end("Here's our courses available");

    } else if(request.url == "/addcourse" && request.method == "POST"){
        response.writeHead(200,{"content-type" : "text/plain"});

        response.end("Add a course to our resources");

    }else if(request.url == "/updatecourse" && request.method == "PUT"){
        response.writeHead(200,{"content-type" : "text/plain"});

        response.end("Update a course to our resources")

    } else if(request.url == "/archivecourses" && request.method =="DELETE"){
        response.writeHead(200,{"content-type" : "text/plain"});

        response.end("Archive courses to our resources")
    } else {
        response.writeHead(200,{"content-type" : "text/plain"});

        response.end("Welcome to Booking System")
    }
    
}).listen(4000);

console.log("Running at localhost:4000");




















//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
