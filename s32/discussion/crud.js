const http = require("http");

let directory = [
    {
        "name" : "kendrick",
        "email" : "kendrick@email.com"
    },
    {
        "name" : "travis",
        "email" : "travis@email.com"
    }
]

http.createServer(function(request, response) {
    if(request.url == "/users" && request.method == "GET"){
        response.writeHead(200,{"content-type" : "application/json"}); 
        //application/json - sets response output to JSON data type

        response.write(JSON.stringify(directory))
        //response.write() - is a node.js method that is used to write data to the response body in a http server
        response.end(`request end`);
    };

    //POST method
    if(request.url == "/addUser" && request.method == "POST"){
        
        //declare and initialize a "requestBody" variable to an empty string
        let requestBody = "";

        //request.on() is an event listener in node.js that is used to handle incoming data in http server
        request.on("data", function(data){
            requestBody += data;
        });

        request.on("end", function(){
            //converts the string requestBody to JSON
            requestBody = JSON.parse(requestBody);
            
            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }
            //use push method into the mock database
            directory.push(newUser);
            console.log(directory);

            response.writeHead(200,{"content-type" : 
            " application/json"});
            response.write(JSON.stringify(newUser));
            response.end();
        });


    }
    

}).listen(4001);

console.log("Running at localhost:4001");