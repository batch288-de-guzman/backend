const http = require("http");

http.createServer(function(request, response) {

    if(request.url == "/items" && request.method == "GET"){
        response.writeHead(200,{"content-type" : "text/plain"});

        response.end("Data retrieved from the database");
    };

    if(request.url == "/items" && request.method == "POST"){
        response.writeHead(200,{"content-type" : "text/plain"})

        response.end("Data to be sent from the database ")
    }

}).listen(4000);

console.log("Running at localhost:4000");