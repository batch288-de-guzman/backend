console.log("JSON Derulo");

// JSON object
    //JavaScript Object Notation
    //json is also used in other programming languages hence the name
    // core javaScript has a built in json object that contains method for parsing objects and converting strings into javascript objects
    //serialization is the process of converting data into series of bytes for easier transmission/transfer of information data

//JSON Arrays
/*
    "cities" : [
        {"City": "Quezon City",
        "Province" : "Metro Manila",
        "Cunty" : "Phippines"    
    }
    ]
*/

//JSON Methods
    //json objects contains methods for parsing and converting data into strigified JSON
    //converting data into stringified json
        //stringified json is a javascript object converter into a string to be used in other function of a javascript application
        //they are commonly used in http request where information is requirerd to be sent and recieve in a stringified version
        // requests are an important part of programming where application communicates with backend application to perform different tasks such as getting/creating data in a database.

let batchARR = [
    {
        batchName : "Batch X"
    },
    {
        batchName: "Batch Y"
    }
];

console.log(batchARR);

console.log("results from stringify method: ");
console.log(JSON.stringify(batchARR));

    //using stringify method with variables

/*let firstName = prompt("What is your name?");
let lastName = prompt ("What is your las name?");
let age = prompt("what is your age?");
let address = {
    city: prompt("Which city are you from? "),
    country: prompt("Which country is that city from?")
};

let otherData = JSON.stringify({
    firstName : firstName,
    lastName: lastName,
    age: age,
    address: address
});
console.log(otherData);
*/

    //converting stingified into javascript object
    //obejcets are common data types used in applications because of the complex data structures that can be created out of them

let batchesJSON = '[{"batchNumber": 199}, {"batchname": "Batch Y"}]'

console.log(batchesJSON)
console.log("result from parse method")
console.log(JSON.parse(batchesJSON));


let exampleObject='{"name" : "Chisa", "age": 19, "isTeenager": false}'

console.log(JSON.parse(exampleObject));
let exampleParse = JSON.parse(exampleObject);
console.log(typeof exampleParse.name);
console.log(typeof exampleParse.age);
console.log(typeof exampleParse.isTeenager);