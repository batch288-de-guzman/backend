const { request } = require("http");
const http = require("http");


//create a variable port to store the port number
const port = 8888;

const server = http.createServer((request, response) =>{
    if(request.url == "/greetings"){
        response.writeHead(200,{"context-type" : "text/plain"})

        response.end("hello bbby girl");
    } else if (request.url == "/homepage"){
        response.writeHead(200, {"Content-type": "Text/plain"})
        
        response.end("this is the homepage");
    } else {
        response.writeHead(404, {"content-type" : "text/plain"})
        
        response.end("hey this page is not available")
    }
})

server.listen(port)

console.log(`server is now accessible at localhost: ${port}.`)