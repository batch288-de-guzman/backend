//"require" directive loads node.js modules
//a modules is a software component or part of a program that contains one or more routines 
//the "http" module lets node.js transfer data using the hyper text transfer protocol
//https is a protocol that allows the fetching of resources such as html documents

//clients and servers communicate by exchanging individual messages
//the messages are sent by the client, usually a web browser and called "request"

//the messages sent by the server as an answer are called "response"
let http = require("http");


//using this module's ceateServer() methodm, we cann create an http server that listens to request on a specified prot and give response back to the client

//the http module has a createServer() methodd that accepts functions as an argument and allows for creaton of a server
//the arguments passed in the function are request and respnse object (data types) that containts methods that allows us tp recieve requests from the client and send responses back to it

http.createServer(function (request, response) {
    //use writehead() method to:
    //set status code for the response - 200 means OK
    //ser the content-type of the response as a plain text message
    response.writeHead(200, {"content-type" : "text-plain"});

    //sends the response with text content "hello worlds?!"
    response.end("NIGGA")
}).listen(8000);

console.log("Server running at localhost:8000")