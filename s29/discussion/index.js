//advanced queries
//querry an embedded document / object

db.users.find({
    contact: {
        phone: "87654321",
        email: "stephenhawking@gmail.com"
    }
})

db.users.find({
    contact: {
        email: "stephenhawking@gmail.com"
    }
})


//  > use dont notation
db.users.find({
    "contact.email" : "stephenhawking@gmail.com"
})


//queying an array with exact element
db.users.find({courses: ["CSS", "Javascript", "Phython"]});

db.users.find({courses:{$all:["React"]}});

//Query Operators
//comparison query operators
    //$gt/$gte operator
        /*
            it allows us to find documents thathave field number values greater or equal to a specified value
            Note that this operator will only work if the data type of the field is number or integer

        */
db.users.find({age:{$gt:72}});

    //$lt/$lte operator
    /*allows us to find document that have field number values less that ot equal to a specified value
    note: same with $gt/$gte operator this will only work if that data type of the field being queried is number or int  */

db.users.find({age:{$lt :76}});

    //$ne operator
    /*allows us to find document that have field nunmberrvalue that is not equal to the specified value */
db.users.find({age:{$ne: 76}});

    //$in operator
    /*
        $in operator allows us to find documents with specific match criteria on field using different values
    */
db.users.find({lastName: {$in : ["Hawking", "Doe"]}});

db.users.find({"contact.phone" : {$in : ["87654321"]}});

db.users.find({courses : {$in: ["React"]}});

//logical query operators
    //$or operator
    /*
        allows us to find documents that match a single criteria from multiple provided search criteria
    */
db.users.find({ $or : [{firstName: "Neil"}, {age: 25}]});

//multiple operators
db.users.find({ $or : 
    [{firstName: "Neil"},
    {age: {$gt : 25}}
    ]
});

    //$and operator
    /*
    allows us to find documents matching all the multiple criteria in a single field
    */

db.users.find({
    $and:[
        {age: {$ne : 82}},
        {age: {$ne : 76}}
    ]
 });

 //mini activity

db.users.find({
    $and : [
        {age: { $gt : 30}},
        {courses: {$in:["CSS", "HTML"]}}
    ]
});


//field projection
    /*
    retirieving documents are common operations that we do and by default mongodb queries return the whole document as a response
    */

//inclusion
    /*
    allws us to include or add specific fields only when retrieving documents   
    */
db.users.find(
    {firstName: "Jane"},
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }
);

db.users.find(
    {firstName: "Jane"},
    {
        firstName: 1,
        lastName: 1,
        "contact.phone": 1
    }
);

//all  entries, and their specific fields
db.users.find(
    {},
    {
        firstName: 1,
        lastName: 1,
        "contact.email": 1
    }
);

//exclusions
/*
    allows us to exclude/remove specific fields only when retieing documents
*/

db.users.find(
    {firstName: "Jane"},
    {
        contact: 0,
        department: 0
    }
);

//dot notation to target object
db.users.find(
    {firstName: "Jane"},
    {
        "contact.email": 0,
        department: 0
    }
);

db.users.find(
	{ $or: 
		[ { firstName : "Jane"},
			{age : {$gte : 30}}
			]
		},

	{
		"contact.email": 0,
		department: 0
	}
	)


    db.users.find(
        { $or: 
            [ { firstName : "Jane"},
                {age : {$gte : 30}}
                ]
            },
    
        {
            "contact.email": 0,
            department: 0,
            courses: { $slice : 2}
        }
        )
    

//evaliation query operator
    //$regex operator
    /**
     allow us to find documents that match a specific string pattern using regular expression
     */
//case sensitive
db.users.find({firstName: {$regex: "en"}});
//vase insensitive
db.users.find({firstName: {$regex: "n", $options: "i"}});