console.log("gods plan");

//if, else if, else statement

let numA = -1

if(numA<0){
    console.log("hello");
}

// the result of the expression in the if's condition must result true, ekse, the statement inside the block {} will not run

// lets update thr variable and run an if statement with the same condition

numA = 0;
if(numA <0 ){
    console.log("hello again if numA is 0")
}

//it will not run becuase the expression resulted false
console.log(numA < 0);

let city = "New York";

if(city === "New York"){
    console.log("Welcome to New York");
}

//else if statements execute a previous statement if previous condition are false and if specified condition is true
//the else if statement is optional and can be added to capture additional conditions to change the flow of a program

let numH = 1;
if(numH > 2){
    console.log("hello");
}
else if(numH < 2){
    console.log("world");
}

// we were able to run the else if statement after we evaluated that the if condition was false

numH = 2

if(numH === 2){
    console.log('hello');
}
else if(numH > 2){
    console.log('worlds')
}

city = "Tokyo";
if(city === "New York"){
    console.log("hello");
}
else if( city === "Manila"){
    console.log("kamusta")
}

else if(city === "Tokyo"){
    console.log("konichiwa");
}

//since we failed the condition for the if and  first else if, we went to the second else if and checked true

//else statement executes a statement if all other conditions are false
//the else statement is optional and can be added to capture any other possible result change to the program

let numB = 0;
if(numB > 0){
    console.log("Hello from numB")
}
else if (numB < 0){
    console.log("World from B")
}
else{
    console.log("again")
}

//since preceeding if and else if conditions failed, the else statement ran instead

// if else statements with fuctions. most of the times we would like to use else statements with functions to control the flow of our program.

function determineTyphoonIntensity(windSpeed){
    if (windSpeed < 0){
        return "Invalid wind speed"
    }
    else if(windSpeed <= 38 && windSpeed >= 0){
        return "Tropical Depression Detected"
    }
    else if(windSpeed <= 73 && windSpeed >= 39){
        return "Tropical Storm Detected"
    }
    else if(windSpeed >= 74 && windSpeed <= 95){
        return "Category 1 Detected"
    }
    else if(windSpeed >= 96 && windSpeed <= 110){
        return "Category 2 Detected"
    }
    else if(windSpeed >= 111 && windSpeed <= 129){
        return "Category 3 Detected"
    }
    else if(windSpeed >= 130 && windSpeed <= 156){
        return "Category 4 Detected"
    }
    else{
        return "Category 5 Detected"
    }
    
    
}
console.log(determineTyphoonIntensity());

//console.war is a good way to print awarning in our console that would help us developers act on a certain output
console.warn(determineTyphoonIntensity());

//thruty and falsy
//a truthy value is a value that is considered true when encountered in a boolean context.
//falsy walues /exemptions for truthy:
/*
    1   false
    2   0
    3   -0
    4   ""
    5   null
    6   undefined
    7   NaN
*/

// truthy example
if(true){
    console.log("truthy");
}

if(0){
    console.log("falsy");
}

//Contional (Ternary) Operator
/*
ternary operator takes in 3 operands
    1 condition
    2 expression to execute if the condition is truthy
    3 expression to executre if the condition is falsy

it can be used to an if else statement
ternary operators have an implicit return statement meaning eithout "return" the resulting expression can be stored in a variable
*/


//single line statement
let ternaryResult = (1 < 18) ? true : false;

console.log("reult of ternary operator: " + ternaryResult);

let exampleTernary = (0) ? "The number is not equal to 0" : "the number is equal to 0 "

console.log(exampleTernary);

//miltiple line execution
function isLegalAge(){
    let name = "john"
    return "you are in the legal age limit, " + name
}
function isUnderAge(){
    let name = "john"
    return "you are under the legal age limit, " + name;
}

/*let age = parseInt(prompt("What is your age?"));
console.log(age)

let legalAge = (age > 18) ? isLegalAge() : isUnderAge();

console.log(legalAge);*/


//switch statement evaluates an expression and muatches the expression's value to a case clause. the switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case

/*let day = prompt("what day of the week is today?").toLowerCase();

switch(day){
    case "monday":
        console.log("the color of the day is red");
        break;
    case "tuesday":
        console.log("the color of the day is orang");
        break;
    case "wednesday":
        console.log("the color of the day is yello");
        break;
    case "thursday":
        console.log("the color of the day is grin");
        break;
    case "friday":
        console.log("the color of the day is bloo");
        break;
    case "saturday":
        console.log("the color of the day is indgido");
        break;
    case "sunday":
        console.log("the color of the day is voilet");
        break;

    default:
        console.log("Pls input a normal day")
        break;
}
*/

//try - catch - finally statement
// try catch statements are commonly used for error handling. there are instances when the application returns an error. warning that is not nevessarily an error in context of our code

function showIntensityAlert(windSpeed){
    try{
        alert(determineTyphoonIntensity(windSpeed))
    }
    catch(error){
        console.warn(error.message);
    }
    finally{
        alert('intensity updates will show new alert!');
    }
}

showIntensityAlert(20);

console.log("im from after showInternsityAlert");