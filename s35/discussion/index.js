const express = require('express');

//mongoose is a package that allows creation of schemas to model our data structure
//also has access to a number of methods for manipulation our database
const mongoose = require('mongoose');

const port = 3001;

const app = express();


    //section mongodb connection

    //connect to the database by passong your connection string
    //due to update in mongodb drivers that allow connection, the default colelction string is being flagged as an error
    //by default a warning will be displayed in the terminal when the application is running
    //{newUrlParses:true}

    //syntax:
    /*mongoose.connect("MongoDB string", {useNewUrlParses:true}); */

    mongoose.connect('mongodb+srv://admin:admin@batch288deguzman.zygjru8.mongodb.net/batch288-todo?retryWrites=true&w=majority', {useNewUrlParser: true});

    //notification wether the connection with the database is established

    let db = mongoose.connection;

    //for catching the error  in case ther isan error during connection
    //console.error.bond allows us to print errors in the browser and in th terminal
    db.on(`error`, console.error.bind(console, `connection error. cant connect to the database`));

    //if the connection is successful;

    db.once(`open`, ()=> console.log(`we are connected to the cloud database`));


        //_______________middleware________

//allows our app to read json data
app.use(express.json())

app.use(express.urlencoded({extended: true}))





    //mongoose schemas
    //schemas dertermince the structure of the document to be written in the database
    //schemas act as blueprint to our data
    //use the schema() constructor of the mongoose module to create new schema object.


    const taskSchema = new mongoose.Schema({
        //defines the fields with the corresponsing data type
        name: String,
        //add another field
        status: {
            type: String,
            default: `pending`
        }
    })


    //models
    //uses schema and are used to instantiate objects that coresponds to the schema
    //models use schema and they act as the middleman from the server(js code) to our database 

    // to create a model we are going to use the model()

    const Task = mongoose.model('Tasks', taskSchema)

    //routes
    //create a psot route to create a new tak
    //careate a new task
    /*
        business logic:
            1. add a fuctionality to check wether there are duplicate task
                -if task is existing in the data base, return ewwow
                -if the task doesnt exist in the database add it in the database
            2. the task data will be coming from the request's body 
    */

    app.post("/tasks", (request, response) =>{
        //finOne is a mongoose method that actis simili to find in mongodb
        Task.findOne({name: request.body.name})
        .then(result => {
            if(result !== null){
                return response.send(`duplicate task found!`)
            }else{
                let newTask = new Task({
                    name: request.body.name
                })

                //the save() methods will store the information to the database
                //since the newTask was created from the mongoose schema and task model, it will be saved in task collection
                newTask.save()

                return response.send(`new task created`)
            }
        })

    })


    //get all the task in our collection
    //1. retirieve all the documents
    //2. if an error is encountered, print the error
    //3. if no errors are found, esnd a success status to the client and show the documents retirieved

    app.get('/tasks', (request, response)=>{
        Task.find({}).then(result =>{
            return response.send(result);
        }).catch(error => response.send(error))
    })
//________Activity_________



const userSchema = new mongoose.Schema({
    
    username: String,
    password: String
})

const User = mongoose.model('User', userSchema)

app.post(`/signup`, (request, response) =>{
    User.findOne({username: request.body.username}).then(result =>{
        if(result !== null){
            return response.send(`Duplicate username found`)
        } else{
            if(request.body.username !== "" && request.body.password !== ""){
                let newUser = new User({
                    username: request.body.username,
                    password: request.body.password
                })
    
                newUser.save()
                
                return response.send(`New user registered`)
            }else{
                return response.send(`BOTH username and password must be provided`)
            }
        }
    })
})








//_________________________
if(require.main === module){
    app.listen(port, ()=>{
        console.log(`server running port ${port}`)
    })
}

module.exports=app;