// console.log("ifuudoudou");

//exponent operator
    //before es6 update
    //Math.pow(base,exponent)

    const fristNum = Math.pow(8,2);
    console.log(fristNum);

    const secondNum = Math.pow(5, 5);
    console.log(secondNum)

    //after es6update

    const thirdNum = 8**2;
    console.log(thirdNum);

    const fourthNum =  5**5;
    console.log(fourthNum);

// template literals
//it will allow s to write stringswithout using concatenation operator
//greatly helps with the code readability

    //before es6
    let name = "tsunko";
    let message = "seeeeegs " + name + " ore wa segs ni natta!!!";
    console.log(message);

    //after es6
    message = `seeeegs ${name}! ore wa segs ni natta!!!`
    console.log(message);

    //multiline using template literal

    const anotherMessage = `${name} attended a cosplay competition
    She won by cosplaying Raiden Shogun, thirst-trapping the judges`
    console.log(anotherMessage);

    const interestRate = 0.1;
    const principal = 1000;

    console.log(`the interest on your savings account is : ${interestRate * principal}`)

//array destructuring
/*
    it allows us to unpack elements in array into disctinct variables
    syntax:
        let/const [varNmaeA, varNmaeb, varNmaeC, ... ] = arrayName
*/

const fullName = ["Juan", "Dela", "Cruz"]
//before es6
// let fristName = fullName[0]
// let secondName = fullName[1]
// let lastName = fullName[2]

// console.log(`Hello ${fristName} ${secondName} ${lastName}! nice to meet ya`);

//after es6 update:
    const [name1, name2, name3] = fullName;
    console.log(name1);
    console.log(name2);
    console.log(name3);

    let gradesPerQuarter = [98, 97, 95, 94];
    console.log(gradesPerQuarter);

    let [firstGrading, secondGrading, thirdGrading, fourthGrading] = gradesPerQuarter;
    console.log(firstGrading);
    console.log(secondGrading);
    console.log(thirdGrading);
    console.log(fourthGrading);

//object desstructuring
    //allow us to unpace properties of object into distict variables;
    //shortens the syntax for accessing properties from objects
    //let/const{propertyNameA, propertyNameB, propertyNameC, ...} = objectName

const person = {
    maidenName: "Dela",
    givenName: "Jane",
    
   familyName: "Cruz"
}
console.log(person);

// const givenName = person.givenName;
// const maidenName = person.maidenName;
// const familyName = person.familyName;
// console.log("givenName");
// console.log("maidenName");
// console.log("familyName");

const {maidenName, familyName, givenName} = person;
console.log(person);

console.log(`this is the givenName ${givenName}`);
console.log(`this is the maidenName ${maidenName}`);
console.log(`this is the familyName ${familyName}`);

//arrow function
    //compact alternative syntax to a traditional function
    /*
        syntax:
        const/let varName = () => {
            statement/codeblock
        }
    */

        const hello = () => {
            console.log("segs");
        }

        hello();

        const printFullName = (firstName, middleInitial, lastName) => {
            console.log(`${firstName} ${middleInitial} ${lastName}`)
        }

        printFullName("John", "D", "Smith");
        printFullName("sento", "D", "yukihara");

        const students = ["Arashi", "Judy", "Mary"]

        students.forEach((student)=>{
            console.log(`${student} is a student`)
        })

//omplicit return in arrow function
    //ifa function will run one statement/line, the arrow will implicitly return the value
        const add = (x,y) => x+y;
        let total = add(10,12);
        console.log(total);

//default function argument value



const greet = (name ="user", age=0) => {
    return `good morning ${name} im ${age} years old`;
}

console.log(greet("tatsumaki", 27));

function addnumber(x,y){
    return x+y;
}
console.log(addnumber(1,1))

//class-based object blueprints
    //allow us to create/instantiate objects using a class blueprint
    /*
        class classname{
            constructor(objectPropertyA, objectPropertyB){
                this.objectPropertyA = objectValueA;
                this.objectPropertyB = objectValueB;
            }
        }
    */

class Car {
    constructor(par1, par2, par3){
        this.brand = par1;
        this.name = par2;
        this.year = par3;

    }
};

//instantiate an object

const myCar = new Car("Toyoda","toyoda", 2002);
console.log(myCar);