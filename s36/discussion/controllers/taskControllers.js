const Task = require(`../models/task.js`);
const { request } = require("../server.js");

//controllers
//this controller will retrieve all the documents from the tasks collections

module.exports.getAllTasks = (request, response) => {
    Task.find({})
    .then(result=> {
        return response.send(result);
    })
    .catch(error => {
        return response.send(error);
    })
}

//create a controller that will add a data in the database

module.exports.addTasks = (request, response) => {

    Task.findOne({"name" : request.body.name})
    .then(result =>{
        if(result !== null){
            return response.send(`Duplicate Task`)
        }else{
            let newTask = new Task({
                "name" : request.body.name
            })
            newTask.save();

            return response.send(`new task created`)
        }
    }).catch(error => response.send(error))
}

module.exports.deleteTask = (request, response) =>{
    console.log(`${request.params.id} ID has been deleted`);

    let taskToBeDeleted = request.params.id;

    //in mongoose, we have the findbyidandremove method that will look for a document with the same id provided from the url and remove the document from mongodb
    Task.findByIdAndRemove(taskToBeDeleted)
    .then(result =>{
        return response.send(`the document that has the id of ${taskToBeDeleted} has been deleted`)
    }).catch(error => response.send(error));
}


//____________activity____________

module.exports.specificTask = (request, response) =>{
    let taskGet = request.params.id;
    console.log(taskGet)
    

    Task.findById(request.params.id)
    .then(result =>{
        return response.send({result})
    }).catch(error => response.send(error));
}


module.exports.updateTask = (request, response) =>{

    Task.findByIdAndUpdate({_id: request.params.id}, request.body)
    .then(result => {
        Task.findOne({_id: request.params.id})
        .then(result=>{
            response.send(result)
        })

    }).catch(error => response.send(error))
}