const express = require(`express`);

const taskControllers = require(`../controllers/taskControllers.js`);



//contain all the enpoints of the application
const router = express.Router();

router.get(`/`, taskControllers.getAllTasks);

router.post(`/addTask`, taskControllers.addTasks);

//parametarized
//create a route using a delete method at the url `/tasks/:id`

//the colon is an identifier that helps create a dynamin route which allows us to supply information
router.delete(`/:id`, taskControllers.deleteTask);


//_________avtivity_________

router.get(`/:id`, taskControllers.specificTask);

router.put(`/:id/complete`, taskControllers.updateTask);


module.exports= router;