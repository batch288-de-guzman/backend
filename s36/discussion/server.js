const express = require(`express`);
const mongoose = require("mongoose");
const taskRoutes = require(`./routers/taskRoutes.js`);


const port = 4000;

const app = express();

//set up mongodb connection
mongoose.connect(`mongodb+srv://admin:admin@batch288deguzman.zygjru8.mongodb.net/batch288-todo?retryWrites=true&w=majority`, {useNewUrlParser: true})

//contain connection to variable
const db = mongoose.connection;
    db.on(`error`, console.error.bind(console,`Error, cant connect to the db`));

    db.once(`open`, ()=>console.log(`we are now connected to the db`))



app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(`/tasks`, taskRoutes);

if(require.main === module) {
    app.listen(port, () => console.log(`the server is running at port ${port}`))
};

module.exports = app;