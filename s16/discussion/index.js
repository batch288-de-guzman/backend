// console.log("sicko mode");

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("result of addition operation: " + sum);

let difference = y - x;

console.log("result of subtraction operation: " + difference);

let product = x * y;
console.log("result of multiplication operator: " + product);

let quotient = y / x;
console.log("result of division operator: " + quotient);

let remainder = y % x;
console.log("result of modulo operator: " + remainder);

let assignmentNumber = 8;

assignmentNumber += 2;
console.log(assignmentNumber);

assignmentNumber += 3;
console.log("result of addition assignment operator "+ assignmentNumber);

assignmentNumber -= 2;
console.log("result of subtraction assignment operator " + assignmentNumber);


assignmentNumber *= 3;
console.log("result of multiplication assignment operator " + assignmentNumber);

assignmentNumber /= 11;
console.log("result of division assignment operator " + assignmentNumber);


// MDAS- multiplication or division first, then addition or subtraction, from left to right
let mdas = 1 + 2 - 3 * 4 / 5;

/*
1. 3*4 = 12
2. 12/5 = 2.4
3. 1+2=3
4. 3-2.4=0.6
 */

console.log(mdas.toFixed(1));

let pemdas = 1 + (2-3) * (4/5);

/*
    1. 4/5 = 0.8
    2. 2-3 = 1
    3. 1 + (-1) * (0.8)
    4. -1 * 0.8 = -0.8
    5. 1 - 0.8
    0.2
*/

console.log(pemdas.toFixed(1));

let z = 1;

let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

increment = z++;
console.log("result of psot increment: " + increment);
console.log("result of psot increment: " + z);

x = 1;
let decrement = --x;
console.log("result of pre decrement: " + decrement);

decrement = x--;
console.log("result of post decrement: " + decrement);
console.log("result pf post decrement: " + x)

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

let numE = true + 1;
console.log(numE);

let numF = false +1;
console.log(numF);

let juan = 'juan';

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('JUAN' == 'juan');
console.log(juan == 'juan');

console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('JUAN' != 'juan');
console.log(juan != 'juan');

console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' ===  'JUAN');

console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('JUAN' !== 'juan');
console.log(juan !== 'juan');


let a = 50;
let b = 65;

let isGreaterThan = a > b;
console.log(isGreaterThan);

let isLessThan = a < b;
console.log(isLessThan);

let isGTorEqual = a > b;
console.log(isGTorEqual);

let isLTorEqual = a <= b;
console.log(isLTorEqual);

let numStr = '30';
console.log(a > numStr);
console.log(b <= numStr);

let strNum = 'twenty';
console.log(b >= strNum);

let isLegalAge = true;
let isRegistred = false;

let allRequirementsMet = isLegalAge && isRegistred;


console.log("result of locial AND operator: " + allRequirementsMet)

let someRequirementsMet = isLegalAge || isRegistred;
console.log("result of locial OR operator: " + someRequirementsMet);

let someRequirementsNotMet = !isRegistred;
console.log("result of locial NOT operator: " + someRequirementsNotMet);