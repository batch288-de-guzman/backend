// console.log("d-d-d-dj khaled!")

//

let studentNumbers=['2020-1923', '2020-1924', '2020-1925', '2020-1926','2020-1927'];

console.log(studentNumbers.length);


let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["acer", "asus", "lenovo", "Noe", "Redfox", "toshiba"];

//posible used of an array but is not reccomended

let mixedArr = [12, "asus", "null", undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

let myTasks = [
    "drink html",
    "eat javascript",
    "inhale css",
    "bake css"
];

console.log(myTasks);

let city1 = "tokyo"
let city2 = "manila"
let city3 = "jakarta"

let cities = [city1, city2, city3];

console.log(grades[0]+grades[1])


let blankArr = [];
console.log(blankArr);

// .lenght property can also set the total number of elements in an array
console.log(myTasks);

// myTasks.length = myTasks.length -1;
myTasks.length -= 1;
console.log(myTasks);
console.log(myTasks.length);

//to delete a specific item we can employ array methods

console.log(cities);
cities.length--;
console.log(cities);

let fullName = "Jamie Clark";
console.log(fullName);
fullName.length -= 1;
console.log(fullName);

// since you can shorten the array by setting the lenght property, you can also lenghten it by adding a number into the lenght property. since we can lengeten the array forcible, there will be another item in the arry, however, it will be empty or undefined

let theBeatles = ["john", "ringo", "paul", "george"]

theBeatles.length += 1;
console.log(theBeatles);

console.log(grades[0]);

let lakersLegends = ["kobe", "shaq", "lebron", "magic", "kareem"]

let currentLaker = lakersLegends[2];
console.log(currentLaker);

console.log("array before reassignment")
console.log(lakersLegends);

lakersLegends[1] = "kai sotto"
console.log(lakersLegends);


//accessing the last element of an array
//since the elements of an array starts by 0, subtracting 1 to the lenght of an array will offsett the value by one allowing us to get the last element

let bullsLegend = ["jordan", "pippen", "rodman", "rose", "kukoc"];

console.log(bullsLegend)
console.log(bullsLegend[bullsLegend.length-1])

let newArr = [];
console.log(newArr);
newArr[newArr.length] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length] = "Tifa Lockhart";
console.log(newArr);

//adds the new array in the next empty element
newArr[newArr.length] = "Goku";
console.log(newArr);

console.log("_______________________");

//looping over an array
//you can use a for loop to iterate over all items in an array

let numDivBy5 = [];
let numNotDivBy5 = [];
for(let index = 0; index < bullsLegend.length; index ++){
    console.log(bullsLegend[index]);
}

let numArr = [5, 12 ,30, 46, 40];

for(let index = 0; index < numArr.length; index ++){
    if (numArr[index] % 5 === 0 ){
        console.log(numArr[index] + " is divisible by 5");
        numDivBy5[numDivBy5.length] = numArr[index];
    } else{
        console.log(numArr[index] + " is not divisible by 5");
        numNotDivBy5[numNotDivBy5.length] = numArr[index]
    }
   
}

console.log(numDivBy5);
console.log(numNotDivBy5);


console.log(" ");


//multi dimentional array
//multi dimentional arrays are useful for storing complex structure
//though useful in number of cases creating complex array ctructures is not always reccommended

let chessboard =  [
['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
['a1', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']]

console.log(chessboard);



console.table(chessboard);

