let myVar;



let productName= "desktop computah";



let price = 18999;


const interest = 3.599;

productName = 'laptop';



// interest = 14.99;

console.log(interest);


// initializing
let supplier;
supplier='jack';

// reassigning
let consumer = "christ";

consumer = "topher";


let globalVariable;
    {
        let innerVariable = "hello there";
        
        console.log(innerVariable);

        globalVariable="ayeiiuuu";
    }

console.log(globalVariable)


let productCode = "DC07", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

console.log(productCode, productBrand);


//strings

let country ='Philippines';
let province ="Metro Manila";
//concatination of strings

let fullAddress = province +", "+ country;

console.log(fullAddress);

let greeting ='i live in the ' + country
console.log(greeting);

// \n escape character moves string to the next line
let mailAddress = "metro manila\n\nphilippines";
console.log(mailAddress);

let message = "'john's employees went hoome early";
console.log(message);

message = 'john\'s employees went home early';

//numbers
//integers whole numbers
let headcount = 29;
console.log(headcount);

// decimal/fraction numbers
let grade = 89.7;
console.log(grade);

//exponential notation

let planetDistance = 2e10;
console.log(planetDistance);

//combination of text and numbers
console.log("jonhs grade last quarter is " + grade);

//boolean
//boolean values are normally used to create values relating to the statr of centain things

let isMarried = false;
let isGoodCunduct = true;

console.log("isMarried: " +isMarried);
console.log(isGoodCunduct);

//arrays
//arrays are special kind of data that's used to store miltiple related values
//arrays can store different data types but is normally use to store similar types

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades); 

let details = ["john", "smithy", 23 , true];
console.log(details);

//objects are another special kind of data type thats use to momoc real world objects/items

let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["+63917 123 4567", "8123 4567"],
        address: {
            houseNumber: '345',
            city: 'Manila'
        }
}

console.log(typeof person);
console.log(typeof headcount);
console.log(typeof grades);

const anime = ["One Piece", "One Punch Man", "Shingeki no Kyojin"];
console.log(anime);
// anime = ["One Piece", "One Punch Man", "Kimetsu no Yaiba"];
   
anime[2] = "Kimetsu no yaiba"
console.log(anime);

let spouse = null;

spouse = 'maria';

console.log(spouse);

let fullname