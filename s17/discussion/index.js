console.log('sicko mode');

function printName(){
    console.log("waga nawa Jack")
};

printName();

declaredFunction();

function declaredFunction() {
    console.log("konichiwa sekai");
}

let variableFunction = function(){
    console.log("konichiwa again");
}

variableFunction();

let funcExpression = function funcName(){
    console.log('hello from the other world')
}

funcExpression();

declaredFunction = function(){
    console.log('updated declaredFunction')
};

declaredFunction();

funcExpression = function(){
    console.log('updated funcExpression')
};

funcExpression();


const constantFunc = function(){
    console.log("initialized with const");
};

constantFunc();

/*constantFunc = function(){
    console.log('kinilaw');
}*/

{
    let localVar = "Travis Scott";
}
// console.log(localVar);

let globalVar = "Post Malone";
{
    console.log(globalVar);
}

//function scope
function showNames(){
    const functionConst = 'judy';
    let functionLet = 'mary'

    console.log(functionConst);
    console.log(functionLet);
}

// console.log(functionConst);
// console.log(functionLet);

showNames();

function myNewFunction(){
    let name = "judy";

    function nestedFunction(){
        console.log(name);
    }
    nestedFunction();
}

myNewFunction();

let functionExpression = function(){
    function nestedFunction(){
            let greetings = "shallom";
            console.log(greetings);
    }
    nestedFunction();
}

functionExpression();

function returnFullName(){
    let fullName = "jesus h. christ";

    return ("christ");
}

returnFullName();

console.log(returnFullName());

let fullName = returnFullName();
console.log(fullName);

function getCourses(){
    let courses = ['subject', "Science 101", "Math 101", "English 101"];
    return courses;
    console.log(courses);
}

getCourses();
console.log(getCourses());


function get(){
    let name = "Jamie clark";
    return name;
}

function foo(){
    return 25%5;
}

function displayCarInfo(){
    console.log("Brand: toyoda");
    console.log("type: sedan");
}

console.log(displayCarInfo());

